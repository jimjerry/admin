<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Users List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php foreach ($result as $user_data){ ?>
                            <tr id="<?= $user_data['id'];?>">
                                <td><?= $user_data['username'];?></td>
                                <td><?= $user_data['type'];?></td>
                                <td><?= $user_data['email'];?></td>                                
                                <td>
                                    <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></button>
                                </td>                                
                            </tr>
                           
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>


