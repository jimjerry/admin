<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
  $("#date").datepicker({
      dateFormat: 'yy-mm-dd'
  });
  $("#close_date").datepicker({
      dateFormat: 'dd/mm/yy'
  });
});
</script>

<div id="success_msg" style="display: none;" class="alert alert-success">
    <b class="alert-link">Payment has been done.</b>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <?= $result[0]['name'];?>
            </div>
            <div class="panel-body">                
                <div class="col-lg-2 text-center">
                    <img style="width: 100%; height: auto;" src="<?= base_url() ?>assets/images/user_images/<?= $result[0]['client_img'];?>">
                    <br><br>
                    <button id="payment" type="button" class="btn btn-success btn-small" data-toggle="modal" data-target="#client_doc">Payment</button>
                    <button type="button" class="btn btn-danger btn-small" data-toggle="modal" data-target="#deactive_modla">
                        Deactive Account
                    </button>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                       <label>Status : <span class="text-success">Active</span></label>
                    </div>
                    
                    <div class="form-group">
                       <label>Phone no :</label> <?= $result[0]['phone'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Flat :</label> <?= $result[0]['flat_no'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>National card no :</label> <?= $result[0]['nid_no'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Total Member :</label> <?= $result[0]['total_member'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Joinging Date :</label> <?= $result[0]['joining_date'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Electricity A/C :</label> <?= $result[0]['electricity_acc'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Gas Card no :</label> <?= $result[0]['gass_bill_card_no'];?>
                    </div>
                    
                    
                </div>
                <div class="col-lg-8">
                    
                    <div class="form-group">
                       <label>House Rent :</label> <?= $result[0]['house_rent'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Water Bill :</label> <?= $result[0]['water_bill'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Electricity Bill :</label> <?= $result[0]['electricity_bill'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Gas Bill :</label> <?= $result[0]['gas_bill'];?>
                    </div>
                    
                    <div class="form-group">
                       <label>Advance :</label> <?= $result[0]['advance_monay'];?>
                    </div>
                    
                </div>

            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                Payment History
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Month</th>
                                <th>House Rent</th>
                                <th>Electricity Bill</th>
                                <th>Gass Bill</th>
                                <th>Water Bill</th>
                                <th>Others</th>                                
                                <th>Total Bill</th>
                                <th>Received</th>                                
                                <th>Due</th>                                
                                <th>Balace</th>
                                <th>Advance</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php foreach($payment_data as $p_history){ ?>
                            <tr class="odd gradeX">
                                <td>     
                                    <?= date('j, F, Y', $p_history['date']);?>
                                   
                                </td>
                                <td class="center"><?= $p_history['month'];?></td>
                                <td><?= $p_history['house_rent'];?></td>
                                <td><?= $p_history['electricity_bill'];?></td>
                                <td><?= $p_history['gas_bill'];?></td>
                                <td><?= $p_history['water_bill'];?></td>
                                <td><?= $p_history['others'];?></td> 
                                <td><?= $p_history['total_bill'];?></td>
                                <td><?= $p_history['received'];?></td>                                                                
                                <td class="center"><?= $p_history['due'];?></td>
                                <td class="center"><?= $p_history['balance'];?></td>
                                <td class="center"><?= $p_history['advance_monay'];?></td>
                                <td style="width: 200px"><?= $p_history['comments'];?></td>
                            </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <div>
                        <ul class="pagination">
                            <?php
                                $limit = 15;
                                $pagi_page = $pagi_page/$limit;
                                $pagi_page = ceil($pagi_page);
                                
                                for ($x = 1; $x <= $pagi_page; $x++) {                                
                            ?>
                            
                            <li class="paginate_button <?=($x == 1)? 'active': '' ?>">
                                <a class="pagination_cls" href="javascript:void(0)"><?= $x; ?></a>
                            </li>
                            
                            <?php
                                }
                            ?>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!-- Payment Modal -->
<div class="modal fade" id="client_doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Bill Payment</h4>
            </div>
            <div class="modal-body">
                <div>                    
                    <div class="form-group">
                       <label>House Rent :</label>
                       <!--<input id="date" type="hidden" name="" value="<?= date("d/m/Y");?>" class="form-control">-->
                       <input id="house_rent" type="text" name="" value="<?= $result[0]['house_rent'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                       <label>Water Bill :</label>
                       <input id="water_bill" type="text" name="" value="<?= $result[0]['water_bill'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                       <label>Electricity Bill :</label>
                       <input id="electricity_bill" type="text" name="" value="<?= $result[0]['electricity_bill'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                       <label>Gas Bill :</label>
                       <input id="gas_bill" type="text" name="" value="<?= $result[0]['gas_bill'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                       <label>Others :</label>
                       <input id="others" type="text" name="" value="0" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label>Select Month</label>
                        <select id="month" class="form-control">
                            <option value="0">Select</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <p id="month_err" style="display: none;" class="text-danger">Select Month</p>
                    </div>
                    
                    <div class="form-group">
                       <label>Date :</label>
                       <input id="date" type="text" class="form-control">
                       <p id="date_err" style="display: none;" class="text-danger">Date Required </p>
                    </div>
                    <div class="form-group">
                       <label>Due :</label>
                       <input id="due" type="text" name="" value="<?= $result[0]['due'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                       <label>Balance :</label>
                       <input id="balance" type="text" name="" value="<?= $result[0]['balance'];?>" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label>Total :</label> <div id="total_amount"></div>
                    </div>
                    <div class="form-group">
                        <label>Received :</label> <div id="total_amount"></div>
                        <input id="received" type="text" name="" value="" class="form-control">
                        <p id="received_err" style="display: none;" class="text-danger">The Receive Amount is required </p>
                    </div>
                    <div class="form-group">
                        <label>Comments</label>
                        <textarea id="comments" class="form-control"></textarea>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button id="close_payment_modal" style="float: left;" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button id="close_payment_modal" type="button" class="btn btn-success pay_btn" data-dismiss="modal">Pay</button>
            </div>
        </div>
    </div>
</div>

<!-- Deactive Modal -->
<div class="modal fade" id="deactive_modla" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Deactive Account</h4>
            </div>
            <div class="modal-body">
                <div>                    
                    <div class="form-group">
                       <label>End Date</label>
                       <input id="close_date" type="text" class="form-control">
                       <p id="date_err" style="display: none;" class="text-danger">Date Required </p>
                    </div>
                    
                    
                    
                </div>
            </div>
            <div class="modal-footer">
                <button id="close_payment_modal" style="float: left;" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button id="deactive_btn" type="button" class="btn btn-danger">Confirm</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    /* setTimeout(function(){
        $("#success_msg").fadeOut('slow');
    }, 3000); */
    
    $(document).ready(function(){
        
        
        $(".pay_btn").on("click",function(){
            
            var house_rent = parseInt($("#house_rent").val()); 
            var water_bill = parseInt($("#water_bill").val());
            var electricity_bill = parseInt($("#electricity_bill").val());
            var gas_bill = parseInt($("#gas_bill").val());
            var others = parseInt($("#others").val()); 
            
            if(!house_rent){
                house_rent = 0;
            }            
            
            if(!water_bill){
                water_bill = 0;
            }            
            
            if(!electricity_bill){
                electricity_bill = 0;
            }
            
            if(!gas_bill){
                gas_bill = 0;
            }
            
            if(!others){
                others = 0;
            }
            
            var due = parseInt($("#due").val());    
            var balance = parseInt($("#balance").val());    
            var due = parseInt($("#due").val());    
            var balance = parseInt($("#balance").val());    
            var comments = $("#comments").val();    
            var bill_cal = '';

            var total_bill = house_rent+water_bill+electricity_bill+gas_bill+others+due+balance;  

            $("#total_amount").html(total_bill);
            var client_id = '<?= $result[0]['id'];?>';
            
            var date = $("#date").val();
            
            var month = $("#month").find(":selected").val();
            var received = $("#received").val();
            
            if(month == 0){
                $("#month_err").css('display', 'block');
                return false;
            }
            
            if(date == 0){
                $("#date_err").css('display', 'block');
                return false;
            }
            
            if(received == ''){
                $("#received_err").css('display', 'block');
                return false;
            }
            
            received = parseInt(received);
            bill_cal = bill_cal+balance;
            bill_cal = bill_cal+due;
            bill_cal = total_bill-received;            
            
            if(bill_cal == 0){
                 due =0;
                 balance = 0;
                
            }else{
                if(bill_cal > 0){
                    due = bill_cal;
                    balance = 0;
                }else{
                    due =0;
                    balance = bill_cal;
                }
            }
            
            $.ajax({
            type: "POST",
            url: "<?= base_url()?>Admin/payment",
            dataType:"html",
            data:{
                date : date,
                received : received,
                total_bill : total_bill,
                client_id : client_id,
                due : due,
                month : month,
                balance : balance,
                house_rent : house_rent,
                electricity_bill : electricity_bill,
                gas_bill : gas_bill,
                water_bill : water_bill,
                others : others,
                comments : comments
                
                
            },
            success: function(response){
                if(response){                    
                    $("tbody").html(response);                    
                    console.log("Payment Done");
                    $("#success_msg").css("display" , "block");
                    setTimeout(function(){
                        $("#success_msg").fadeOut('slow');
                    }, 3000);
                }
            }});
        });
        
    });   
    
    $(document).on("click", "#close_payment_modal", function(){
        $(".form-group").find("p#received_err").css("display", "none");
        $(".form-group").find("p#month_err").css("display", "none");
    });
    
    $(".modal-body .form-control").on('keyup', function(){
        
         house_rent = parseInt($("#house_rent").val());         
         if(isNaN(parseFloat(house_rent))){             
             house_rent = 0;             
         }
         
         water_bill = parseInt($("#water_bill").val());         
         if(isNaN(parseFloat(water_bill))){
             water_bill = 0;
         }
         
         electricity_bill = parseInt($("#electricity_bill").val());         
         if(isNaN(parseFloat(electricity_bill))){
             electricity_bill = 0;
         }
         
         gas_bill = parseInt($("#gas_bill").val());         
         if(isNaN(parseFloat(gas_bill))){
             gas_bill = 0;
         }
         
         others = parseInt($("#others").val());          
         if(isNaN(parseFloat(others))){
             others = 0;
         }
         
         due = parseInt($("#due").val());         
         if(isNaN(parseFloat(due))){
             due = 0;
         }
         
         balance = parseInt($("#balance").val());         
         if(isNaN(parseFloat(balance))){
             balance = 0;
         }
        
        total_bill = house_rent+water_bill+electricity_bill+gas_bill+others+due+balance;  
        
        $("#total_amount").html(total_bill);
        
    });
    
    $(document).on("click", "#deactive_btn", function(){
        var close_date = $("#close_date").val();
        var flat_no = "<?= $result[0]['flat_no'];?>";
        client_id = "<?= $result[0]['id'];?>";
        
        $.ajax({
            type: "POST",
            url: "<?= base_url()?>Admin/deactivate",
            dataType:"json",
            
            data:{                
                close_date : close_date,
                flat_no : flat_no,                           
                client_id : client_id                           
            },
            success: function(response){
                console.log("deactivate done");
                window.location.replace("<?= base_url()?>Admin/active_client");
        }});
        
    });
    
    $(document).on("click" , ".pagination_cls", function(){
    
        $(".paginate_button").removeClass("active");
        $(this).closest("li").addClass("active");
        
        var start_val = parseInt($(this).text());
        var id = "<?= $result[0]['id'];?>";        
        var limit = <?= $limit;?>;
        
        start_val = (start_val*limit)-limit;

        $.ajax({
            type: "POST",
            url: "<?= base_url()?>Admin/ajax_pagination_data",
            dataType:"html",
            
            data:{                
                id : id,
                start_val : start_val,                           
                limit : limit                           
            },
            success: function(response){
                $("tbody").html(response);
                //$("#success_msg").css("display" , "block");
            }
        });
        
    });
    
</script>