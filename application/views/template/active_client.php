<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Active Clients
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Floor</th>                                
                                <th>Phone</th>
                                <th>Electricity Bill A/C no</th>
                                <th>Gass Bill Card no</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php 
                            $i = 1;                              
                            foreach ($result as $user_data) { 
                            ?>
                            <tr id="<?= $user_data['id']; ?>" class="odd gradeX">
                                <td><?= $user_data['name']; ?></td>
                                <td><?= $user_data['flat_no']; ?></td>
                                <td><?= $user_data['phone']; ?></td>
                                <td class="center"><?= $user_data['electricity_acc']; ?></td>
                                <td class="center"><?= $user_data['gass_bill_card_no']; ?></td>
                                <td class="center">
                                    
                                    <a href="<?= base_url()?>Admin/edit?id=<?= $user_data['id']; ?>">
                                        <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></button>
                                    </a>
                                    <a href="<?= base_url()?>Admin/client_view?id=<?= $user_data['id']; ?>">
                                        <button type="button" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></button>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++;} ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>