<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                Inactive Clients
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Floor</th>                                
                                <th>Phone</th>
                                <th>Joing Date</th>
                                <th>End Date</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php 
                            $i = 1;                              
                            foreach ($result as $user_data) { 
                            ?>
                            <tr class="<?=($i % 2 == 0)? 'odd gradeX' : 'even gradeC'?>">
                                <td><?= $user_data['name']; ?></td>
                                <td><?= $user_data['flat_no']; ?></td>
                                <td><?= $user_data['phone']; ?></td>
                                <td class="center"><?= $user_data['joining_date']; ?></td>
                                <td class="center"><?= $user_data['end_date']; ?></td>                                
                            </tr>
                            <?php $i++;} ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>


