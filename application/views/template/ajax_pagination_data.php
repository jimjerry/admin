<?php foreach($payment_data as $p_history){ ?>
<tr class="odd gradeX">
    <td>     
        <?= date('j, F, Y', $p_history['date']);?>

    </td>
    <td class="center"><?= $p_history['month'];?></td>
    <td><?= $p_history['house_rent'];?></td>
    <td><?= $p_history['electricity_bill'];?></td>
    <td><?= $p_history['gas_bill'];?></td>
    <td><?= $p_history['water_bill'];?></td>
    <td><?= $p_history['others'];?></td> 
    <td><?= $p_history['total_bill'];?></td>
    <td><?= $p_history['received'];?></td>                                                                
    <td class="center"><?= $p_history['due'];?></td>
    <td class="center"><?= $p_history['balance'];?></td>
    <td class="center"><?= $p_history['advance_monay'];?></td>
    <td style="width: 200px"><?= $p_history['comments'];?></td>
</tr>
<?php } ?>