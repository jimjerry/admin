<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Administration</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url(); ?>assets/css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url(); ?>assets/css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?= base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            .panel-default>.panel-heading{
                color: #fff;
                background-color: #428ac9;
                border-color: #428ac9;
            }
            
            .error_msg p{
                color: red;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <?php if(isset($error_msg)){ ?>
                        <p style="text-align: center; line-height: 28px; height: 19px; color: red;"><?=$error_msg;?></p>
                        <?php } ?>
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="<?= base_url();?>Login">                                
                                <div class="form-group">
                                    <input class="form-control username" placeholder="User Name" name="username" type="text" autofocus>
                                    <div id="username_error_msg" class="error_msg"><?= form_error('username');?></div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control password" placeholder="Password" name="password" type="password" value="">
                                    <div id="password_error_msg" class="error_msg"><?= form_error('password');?></div>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" id="login_submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?= base_url(); ?>assets/js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?= base_url(); ?>assets/js/startmin.js"></script>
        <script type="text/javascript">
            
            $(document).on("click", "#login_submit", function(){
                var username = $(".username").val();
                var password = $(".password").val();
                
                if(username == ''){
                    $("#username_error_msg").html("<p>The User Name field is required</p>");                    
                }else{
                    $("#username_error_msg").empty();
                }
                
                if(password == ''){
                    $("#password_error_msg").html("<p>The Password field is required</p>");                    
                }else{
                    $("#password_error_msg").empty();
                }
                
                
                if(username == '' || password == ''){                    
                    return false;
                }
                
                username = username.replace(/[\'^£$%&*()}{@_#~?><>,|=+¬-]/, '');  /* special character fieltering */
                username = username.trim(); /* Triming */
                username = username.replace(/\s/g, ''); /* White space removing */   
                //console.log(username,password);
                //return false
            });
            
        </script>
    </body>
</html>
