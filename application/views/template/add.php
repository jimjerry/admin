<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
  $("#datepicker").datepicker();
});
</script>

<?php if(isset($input_data)){ ?>
<div id="success_msg" class="alert alert-danger">
    <b class="alert-link"><?=$input_data;?></b>
</div>
<?php } ?>
<?php if(isset($success_msg)){ ?>
<div id="success_msg" class="alert alert-success">
    <b class="alert-link"><?=$success_msg;?></b>
</div>
<?php } ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Add New
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" method="post" action="<?=  base_url()?>Admin/add" enctype="multipart/form-data">
                    <div class="col-lg-4">                        
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
                            <div class="text-danger">
                            <?= form_error('name');?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" value="<?=set_value('phone')?>" class="form-control">
                            <div class="text-danger">
                            <?= form_error('phone');?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>National ID no</label>
                            <input type="text" name="nid"  value="<?=set_value('nid')?>" class="form-control">
                            <div class="text-danger">
                            <?= form_error('nid');?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Family Member</label>
                            <input type="text" name="total_member" value="<?=set_value('total_member')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('total_member');?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Joining Date</label>
                            <input type="text" name="joining_date" value="<?=set_value('joining_date')?>" id="datepicker" class="form-control">
                            <div class="text-danger">
                                <?= form_error('joining_date');?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Client Photo</label>
                            <input name="client_img" value="<?=set_value('client_img')?>" class="form-control" type="file">
                            <div class="text-danger">
                            <?= form_error('client_img');?>
                            <?=(isset($error_msg)? $error_msg : '')?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Electricity A/C no</label>
                            <input name="electricity_acc" value="<?=set_value('electricity_acc')?>" class="form-control" type="text">
                            <div class="text-danger">
                                <?= form_error('electricity_acc');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Gass Bill Card no</label>
                            <input name="gass_bill_card_no" value="<?=set_value('gass_bill_card_no')?>" class="form-control" type="text">
                            <div class="text-danger">
                                <?= form_error('gass_bill_card_no');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Flat no</label>
                            <select name="flat_no"  class="form-control">
                                <option value="" selected="selected">Select</option>                                
                                <?php foreach ($flat_arr as $flat){ ?>
                                <option value="<?=$flat['flat_no'];?>" <?= set_select('flat_no',$flat['flat_no'] );?>><?=$flat['flat_no'];?></option>
                                <?php } ?>
                            </select>
                            <div class="text-danger">
                                <?= form_error('flat_no');?>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Monthly House Rent</label>
                            <input type="text" name="house_rent" value="<?=set_value('house_rent')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('house_rent');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Monthly Water Bill</label>
                            <input type="text" name="water_bill" value="<?=set_value('water_bill')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('water_bill');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Monthly Electricity Bill</label>
                            <input type="text" name="electricity_bill" value="<?=set_value('electricity_bill')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('electricity_bill');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Monthly Gas Bill</label>
                            <input type="text" name="gas_bill" value="<?=set_value('gas_bill')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('gas_bill');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Advance Money</label>
                            <input type="text" name="advance_monay" value="<?=set_value('advance_monay')?>" class="form-control">
                            <div class="text-danger">
                                <?= form_error('advance_monay');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
    setTimeout(function(){
        $("#success_msg").fadeOut('slow');
    }, 3000);
    
</script>