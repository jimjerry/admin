<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Administration</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?= base_url(); ?>assets/css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="<?= base_url(); ?>assets/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= base_url(); ?>assets/css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!--<link href="<?= base_url(); ?>assets/css/morris.css" rel="stylesheet">-->

        <!-- Custom Fonts -->
        <link href="<?= base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= base_url();?>">Administration</a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="#"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown navbar-inverse">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?= $this->session->userdata("username"); ?><b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?= base_url();?>Login/loggout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                            </li>
                            <li>
                                <a href="<?= base_url();?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            
                            
                            <li>
                                <a href="#"><i class="fa fa-users fa-fw"></i> Client<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?= base_url();?>Admin/add">Add</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url();?>Admin/active_client">Active</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url();?>Admin/inactive_client">Inactive</a>
                                    </li>
                                </ul>
                            </li>
                            
                            
                            <li>
                                <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?= base_url();?>Admin/add_new_user">Add New</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url();?>Admin/view_users">View Users</a>
                                    </li>
                                    
                                </ul>
                            </li>
                            
                            <li>
                            <!--<li>
                                <a href="#"><i class="fa fa-home fa-fw"></i> House<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?= base_url();?>Admin/house">House</a>
                                    </li>
                                    
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#"><i class="fa fa-home fa-fw"></i> Payment<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="#">Total</a>
                                    </li>
                                    
                                </ul>
                            </li>-->
                            
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Dashboard</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    
                    
                    <?php
                    if(isset($home)){
                        $this->load->view('template/home');
                    }
                    
                    elseif(isset($add)){
                        $this->load->view('template/add');
                    }
                    
                    elseif(isset($active_client)){
                        $this->load->view('template/active_client');
                    }
                    
                    elseif(isset($house)){
                        $this->load->view('template/house');
                    }
                    
                    elseif(isset($client_view)){
                        $this->load->view('template/client_view');
                    }
                    
                    elseif(isset($edit)){
                        $this->load->view('template/edit');
                    }
                    
                    elseif(isset($inactive_client)){
                        $this->load->view('template/inactive_client');
                    }
                    
                    elseif(isset($view_users)){                        
                        $this->load->view('template/view_users');
                    }
                    
                    else{
                       $this->load->view('template/home'); 
                    }
                    
                    ?>
                    
                    
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <!-- Bootstrap Core JavaScript -->
        <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?= base_url(); ?>assets/js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<?= base_url(); ?>assets/js/raphael.min.js"></script>
        <!--<script src="<?= base_url(); ?>assets/js/morris.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/morris-data.js"></script>-->

        <!-- Custom Theme JavaScript -->
        <script src="<?= base_url(); ?>assets/js/startmin.js"></script>

    </body>
</html>
