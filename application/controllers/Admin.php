<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    function __construct() {
        parent::__construct();        
        if (!$this->session->userdata("username")) {
            redirect('Login/loggout');
        }
        
        $this->load->model('General_model'); 
        
        if(time() - $_SESSION['timestamp'] > 900) { 
            
            unset($_SESSION['username'], $_SESSION['email'], $_SESSION['lang']);
            $_SESSION['logged_in'] = false;
            redirect("Login");
            exit;
        }else{
            $_SESSION['timestamp'] = time();
        }
        
    }

    
    public function index() {
        $data['home'] = 'home';
        $this->load->view('default_template', $data);
    }
    
    public function active_client() {  
        $whr = array(
            'status' => 1
        );
        $data['result'] = $this->General_model->get_data('*','client', $whr);        
        $data['active_client'] = 'active_client';
        $this->load->view('default_template', $data);
    }
    
    public function add(){
        
        if(isset($_GET['err'])){
            $data['error_msg'] = $_GET['err'];            
        }        
        if(isset($_GET['input_data'])){
            $data['input_data'] = $_GET['input_data'];
        }        
        
        $data['success_msg'] = $this->input->get('success_msg');
        
        
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('nid', 'nid', 'required');
        $this->form_validation->set_rules('total_member', 'total_member', 'required');
        $this->form_validation->set_rules('joining_date', 'joining_date', 'required');
        $this->form_validation->set_rules('electricity_acc', 'electricity_acc', 'required');
        $this->form_validation->set_rules('gass_bill_card_no', 'gass_bill_card_no', 'required');
        $this->form_validation->set_rules('flat_no', 'flat_no', 'required');

        if (empty($_FILES['client_img']['name'])){
            $this->form_validation->set_rules('client_img', 'Document', 'required');
        }
        
        
        $this->form_validation->set_rules('house_rent', 'house_rent', 'required');
        $this->form_validation->set_rules('water_bill', 'water_bill', 'required');
        $this->form_validation->set_rules('electricity_bill', 'electricity_bill', 'required');
        $this->form_validation->set_rules('gas_bill', 'gas_bill', 'required');
        $this->form_validation->set_rules('advance_monay', 'advance_monay', 'required');

        
        if($this->form_validation->run()){        
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $nid = $this->input->post('nid');
            $total_member = $this->input->post('total_member');
            $joining_date = $this->input->post('joining_date');
            $electricity_acc = $this->input->post('electricity_acc');
            $gass_bill_card_no = $this->input->post('gass_bill_card_no');
            $flat_no = $this->input->post('flat_no');
            
            $house_rent = $this->input->post('house_rent');
            $water_bill = $this->input->post('water_bill');
            $electricity_bill = $this->input->post('electricity_bill');
            $gas_bill = $this->input->post('gas_bill');
            $advance_monay = $this->input->post('advance_monay');
            
            
            /* file upload */
            $target_dir ="assets/images/user_images/";
            
            $user_img = $_FILES["client_img"]["name"];
            $user_img = explode(".", $user_img);
            $user_img = $user_img[0].time().".".$user_img[1];
            
            $target_file = $target_dir . basename($user_img);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));            
            $check = getimagesize($_FILES["client_img"]["tmp_name"]);
            
            if($check !== false) {                
                $uploadOk = 1;
            } else {                
                redirect('Admin/add?err=File is not an image.');
                $uploadOk = 0;
            }            
            
            if (file_exists($target_file)) {
                redirect('Admin/add?err=Sorry, file already exists.');
                $uploadOk = 0;
            }
            
            if ($_FILES["client_img"]["size"] > 500000) { 
                redirect('Admin/add?err=Sorry, your file is too large');
                $uploadOk = 0;
            }
            
            
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                redirect('Admin/add?err=Sorry, only JPG, JPEG, PNG files are allowed.');
                $uploadOk = 0;
            }
            
            
            if ($uploadOk == 0) {
                redirect('Admin/add?err=Sorry, your file was not uploaded.');
            } else {
                
                if (move_uploaded_file($_FILES["client_img"]["tmp_name"], $target_file)) {
                    //echo "The file ". basename( $_FILES["client_img"]["name"]). " has been uploaded.";
                } else {
                    redirect('Admin/add?err=Sorry, there was an error uploading your file.');
                }
            }
            /* file upload end */          
            
            
            $user_data = array(                
                'name' => $name,
                'phone' => $phone,
                'nid_no' => $nid,
                'total_member' => $total_member,
                'joining_date' => $joining_date,
                'client_img' => $user_img,              
                'electricity_acc' => $electricity_acc,              
                'gass_bill_card_no' => $gass_bill_card_no,       
                'flat_no' => $flat_no,  
                'status' => 1  
            );            
            
            $insert_id = $this->General_model->insert_data_with_return_id('client', $user_data);
            
            if($insert_id){
                $user_bill_info = array(
                    'house_rent' => $house_rent,
                    'water_bill' => $water_bill,
                    'electricity_bill' => $electricity_bill,
                    'gas_bill' => $gas_bill,
                    'advance_monay' => $advance_monay,
                    'due' => 0,
                    'balance' => 0,
                    'client_id' => $insert_id
                );
                $result = $this->General_model->insert_data('client_bill', $user_bill_info);
                
                if($result){
                    $table = 'flat';

                    $where = array(
                        'flat_no' => $flat_no
                    );
                    $attr = array(
                        'status' => '1'
                    );
                    $this->General_model->update_data($table, $attr, $where);
                    redirect(base_url().'Admin/add?success_msg=Data has been saved');

                }
                else{
                    echo'wrong';die();
                }
                
            }else{
                echo'wrong';die();
            }
            
        }       
        $whr = array(
            'status' => 0
        );
        $data['flat_arr'] = $this->General_model->get_data('*','flat' ,$whr);        
        $data['add'] = 'add';
        $this->load->view('default_template', $data);
    }
    
    public function house() {
        $data['house'] = 'house';
        $this->load->view('default_template', $data);
    }
    
    public function client_view() {
        $id = $this->input->get('id');
        $data['success_msg'] = $this->input->get('success_msg');
        
        $whr = array(
            'client.id' => $id
        );
        
        $select = "client.id, client.name, client.phone, client.nid_no, client.total_member, client.joining_date, client.client_img, client.electricity_acc, client.gass_bill_card_no, client.flat_no, client_bill.house_rent, client_bill.water_bill, client_bill.electricity_bill, client_bill.gas_bill, client_bill.advance_monay, client_bill.client_id, client_bill.due, client_bill.balance";
        
        $join = array('client_bill', 'client_bill ON client.id = client_bill.client_id', 'INNER');        
        $result = $this->General_model->get_data($select,'client' ,$whr, $join);
        
        if($result){
            $data['result'] = $result;
        }else{
            redirect(("Admin/add?input_data= Wrong Information"));
        }
        
        /* get data from payment table */
        $payment_whr = array(
            'payment.client_id' => $id
        );
        
        $order_by ='payment.date';
        $desc = "DESC";
        
        $join = array('client_bill', 'client_bill ON payment.client_id=client_bill.client_id', 'INNER');
        $sel = "payment.id, payment.date, payment.total_bill, payment.due, payment.balance, payment.received, payment.`month`, payment.client_id, payment.house_rent , payment.electricity_bill, payment.gas_bill, payment.water_bill, payment.others, payment.comments, client_bill.advance_monay";
        $data['payment_data'] = $this->General_model->get_data($sel,'payment' ,$payment_whr, $join, $order_by, $desc, 15, 0);  
        $pagi_page = $this->General_model->get_data('*','payment' ,array('client_id' => $id));  
        $data['pagi_page'] = count($pagi_page);
        //echo "<pre>";print_r($pagi_page);die;
        
        $data['client_view'] = 'client_view';
        $this->load->view('default_template', $data);
    }
    
    public function edit() {
        $id = $this->input->get('id');
        $data['err'] = $this->input->get('err');
        $data['success_msg'] = $this->input->get('success_msg');
        
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('nid', 'nid', 'required');
        $this->form_validation->set_rules('total_member', 'total_member', 'required');
        $this->form_validation->set_rules('joining_date', 'joining_date', 'required');
        $this->form_validation->set_rules('electricity_acc', 'electricity_acc', 'required');
        $this->form_validation->set_rules('gass_bill_card_no', 'gass_bill_card_no', 'required');
        //$this->form_validation->set_rules('flat_no', 'flat_no', 'required');
        
        $this->form_validation->set_rules('house_rent', 'house_rent', 'required');
        $this->form_validation->set_rules('water_bill', 'water_bill', 'required');
        $this->form_validation->set_rules('electricity_bill', 'electricity_bill', 'required');
        $this->form_validation->set_rules('gas_bill', 'gas_bill', 'required');
        $this->form_validation->set_rules('advance_monay', 'advance_monay', 'required');
        
        if($this->form_validation->run()){
            
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $nid = $this->input->post('nid');
            $total_member = $this->input->post('total_member');
            $joining_date = $this->input->post('joining_date');
            $electricity_acc = $this->input->post('electricity_acc');
            $gass_bill_card_no = $this->input->post('gass_bill_card_no');
            //$flat_no = $this->input->post('flat_no');
            
            $house_rent = $this->input->post('house_rent');
            $water_bill = $this->input->post('water_bill');
            $electricity_bill = $this->input->post('electricity_bill');
            $gas_bill = $this->input->post('gas_bill');
            $advance_monay = $this->input->post('advance_monay');
            
            
            if($_FILES["client_img"]["name"] != NULL){
                
                /* file upload */
                
                $user_img = $_FILES["client_img"]["name"];
                $user_img = explode(".", $user_img);
                $user_img = $user_img[0].time().".".$user_img[1];
                
                $target_dir ="assets/images/user_images/";
                $target_file = $target_dir . basename($user_img);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));            
                $check = getimagesize($_FILES["client_img"]["tmp_name"]);

                if($check !== false) {                
                    $uploadOk = 1;
                } else {                
                    redirect('Admin/edit?id='.$id.'&err=File is not an image.');
                    $uploadOk = 0;
                }            

                if (file_exists($target_file)) {
                    redirect('Admin/edit?id='.$id.'&err=Sorry, file already exists.');
                    $uploadOk = 0;
                }

                if ($_FILES["client_img"]["size"] > 512000) {    
                    echo $_FILES["client_img"]["size"];die;
                    redirect('Admin/edit?id='.$id.'&err=Sorry, your file is more than 500kb');
                    $uploadOk = 0;
                }


                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                    redirect('Admin/edit?id='.$id.'&err=Sorry, only JPG, JPEG, PNG files are allowed.');
                    $uploadOk = 0;
                }


                if ($uploadOk == 0) {
                    redirect('Admin/edit?id='.$id.'&err=Sorry, your file was not uploaded.');
                } else {

                    if (move_uploaded_file($_FILES["client_img"]["tmp_name"], $target_file)) {
                        //echo "The file ". basename( $_FILES["client_img"]["name"]). " has been uploaded.";
                    } else {
                        redirect('Admin/edit?id='.$id.'&err=Sorry, there was an error uploading your file.');
                    }
                }
                /* file upload end */
                
                $user_data = array(                
                    'name' => $name,
                    'phone' => $phone,
                    'nid_no' => $nid,
                    'total_member' => $total_member,
                    'joining_date' => $joining_date,  
                    'client_img' => $user_img,     
                    'electricity_acc' => $electricity_acc,              
                    'gass_bill_card_no' => $gass_bill_card_no,       
                    //'flat_no' => $flat_no,  
                    'status' => 1  
                );
            }else{
                $user_data = array(                
                    'name' => $name,
                    'phone' => $phone,
                    'nid_no' => $nid,
                    'total_member' => $total_member,
                    'joining_date' => $joining_date,                    
                    'electricity_acc' => $electricity_acc,              
                    'gass_bill_card_no' => $gass_bill_card_no,       
                    //'flat_no' => $flat_no,  
                    'status' => 1  
                );
            }
            
            $whr = array(
                'id' => $id
            );
            
            $updated_table_1 = $this->General_model->update_data('client', $user_data, $whr);
            
            if($updated_table_1){
                $whr2 = array(
                    'id' => $id
                );
                $user_bill_info = array(
                    'house_rent' => $house_rent,
                    'water_bill' => $water_bill,
                    'electricity_bill' => $electricity_bill,
                    'gas_bill' => $gas_bill,
                    'advance_monay' => $advance_monay                    
                );
                $updated_table_2 = $this->General_model->update_data('client_bill', $user_bill_info, $whr2);
                
                if($updated_table_2){
                    redirect("Admin/edit?success_msg=Data has been updated&id=$id");
                }else{
                    redirect("Admin/edit?err=Data update fail");
                }
            }
            
        
        }
        
        $whr = array(
            'client.id' => $id
        );
        
        $select = "client.id, client.name, client.phone, client.nid_no, client.total_member, client.joining_date, client.client_img, client.electricity_acc, client.gass_bill_card_no, client.flat_no, client_bill.house_rent, client_bill.water_bill, client_bill.electricity_bill, client_bill.gas_bill, client_bill.advance_monay, client_bill.due, client_bill.balance, client_bill.client_id";
        
        $join = array('client_bill', 'client_bill ON client.id = client_bill.client_id', 'INNER');        
        $result = $this->General_model->get_data($select,'client' ,$whr, $join);
        
        if($result){
            $data['result'] = $result;
        }else{
            redirect("Admin");
        }
        
        $data['edit'] = 'edit';
        $this->load->view('default_template', $data);
    }
    
    public function payment() {
        
        if (!$this->input->is_ajax_request()) {
            redirect("Admin");
            exit();
        }
        
        $date = $this->input->post('date');  
        $received = $this->input->post('received');
        $due = $this->input->post('due');
        $balance = $this->input->post('balance');
        $total_bill = $this->input->post('total_bill');
        $month = $this->input->post('month');
        $balance = $this->input->post('balance');
        $client_id = $this->input->post('client_id');
        $house_rent = $this->input->post('house_rent');
        $electricity_bill = $this->input->post('electricity_bill');
        $gas_bill = $this->input->post('gas_bill');
        $water_bill = $this->input->post('water_bill');
        $others = $this->input->post('others');
        $comments = $this->input->post('comments');
        
        $date = date(strtotime($date)); 
        //echo $electricity_bill;die;
        
        $payment_arr = array(
            
            'date' => $date,
            'received' => $received,            
            'total_bill' => $total_bill,
            'month' => $month,
            'due' => $due,
            'balance' => $balance,
            'client_id' => $client_id,
            'house_rent' => $house_rent,
            'electricity_bill' => $electricity_bill,
            'gas_bill' => $gas_bill,
            'water_bill' => $water_bill,
            'others' => $others,
            'comments' => $comments            
        );
        
        $due_n_balance_update = array(
           'due' => $due,
           'balance' => $balance
        );
        
        $this->General_model->update_data('client_bill', $due_n_balance_update, array('client_id' => $client_id));
        
        
        $result = $this->General_model->insert_data('payment', $payment_arr);
//        if($result){
//            echo json_encode($result);
//        }else{
//            die();
//        }
        
         /* get data from payment table */
        $payment_whr = array(
            'payment.client_id' => $client_id
        );
        
        $order_by ='payment.date';
        $desc = "DESC";
        
        $join = array('client_bill', 'client_bill ON payment.client_id=client_bill.client_id', 'INNER');
        $sel = "payment.id, payment.date, payment.total_bill, payment.due, payment.balance, payment.received, payment.`month`, payment.client_id, payment.house_rent , payment.electricity_bill, payment.gas_bill, payment.water_bill, payment.others, payment.comments, client_bill.advance_monay";
        $data['payment_data'] = $this->General_model->get_data($sel,'payment' ,$payment_whr, $join, $order_by, $desc, 15, 0);  
        $pagi_page = $this->General_model->get_data('*','payment' ,array('client_id' => $client_id));  
        $data['pagi_page'] = count($pagi_page);
        
        $this->load->view('template/ajax_pagination_data', $data);
        
    }
    
    public function deactivate(){
        
        $close_date = $this->input->post('close_date');
        $flat_no = $this->input->post('flat_no');
        $client_id = $this->input->post('client_id');
        //echo $close_date."----".$flat_no."----".$client_id;die;
        
        $where = array(
            'id' =>  $client_id,            
        );
        
        $attr = array(
            'status' => 0,
            'end_date' =>  $close_date
        );
        
        $update_client_table = $this->General_model->update_data('client', $attr, $where);
        
        if($update_client_table){
            
            $attr_flat = array(
                'status' => 0
            );
            
            $whr_flat = array(
                'flat_no' =>  $flat_no                
            );
            
            $result = $this->General_model->update_data('flat', $attr_flat, $whr_flat);
            //echo $this->db->last_query();die;
            if($result){
                echo json_encode($result);
            }else{
                echo "Someting Wrong";
                die();
            }
            
        }
        
    }
    
    public function inactive_client() {
        
        $whr = array(
            'status' => 0
        );
        $data['result'] = $this->General_model->get_data('*','client', $whr);  
        
        $data['inactive_client'] = 'inactive_client';
        $this->load->view('default_template', $data);
        
    }
    
    
    public function ajax_pagination_data(){        
        
        if (!$this->input->is_ajax_request()) {
            redirect("Admin");
            exit();
        }
        
        $id = $this->input->post('id');
        $start = $this->input->post('start_val');
        $limit = $this->input->post('limit');
        

        $payment_whr = array(
            'payment.client_id' => $id
        );
        
        $order_by ='payment.date';
        $desc = "DESC";
        
        $join = array('client_bill', 'client_bill ON payment.client_id=client_bill.client_id', 'INNER');
        $sel = "payment.id, payment.date, payment.total_bill, payment.due, payment.balance, payment.received, payment.`month`, payment.client_id, payment.house_rent , payment.electricity_bill, payment.gas_bill, payment.water_bill, payment.others, payment.comments, client_bill.advance_monay";
        
        $data['payment_data'] = $this->General_model->get_data($sel,'payment' ,$payment_whr, $join, $order_by, $desc, $limit, $start); 

        //echo $this->db->last_query();die;
        //echo "<pre>";print_r($result);die;
        $this->load->view('template/ajax_pagination_data', $data);
        
    }
    
    public function view_users() {
        
        $data['result'] = $this->General_model->get_data('*','user');  
        $data['view_users'] = 'view_users';
        $this->load->view('default_template', $data);
    }
    
}