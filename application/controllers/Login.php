<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('General_model'); 
        
    }
	
    public function index(){
        
        if ($this->session->userdata("username")) {
            redirect('Admin');
        }
        
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = explode('Login?error=', $actual_link);
        
        if(isset($actual_link[1])){
            $error_msg = $actual_link[1];
            $error_msg = str_replace('%20', ' ', $error_msg);
        }
        
        
        
        
        $this->form_validation->set_rules('username', 'User Name', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if($this->form_validation->run()){
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            
            $username = self::username_validation($username);
            
            if(!$username){
                redirect('Login?error=Wrong Infomation');
                die();
            }else{
                $whr = array(
                    'username' => $username,
                    'password' => $password
                );
                
                $resultOfData = $this->General_model->query('*', 'user', $whr);
                 
                if($resultOfData){
                    //echo "<pre>";print_r($resultOfData);die;
                    $userData = array(
                        'username' => $resultOfData->username,
                        'email' => $resultOfData->email,
                        'type' => $resultOfData->type,
                        'lang' => 'en',
                        'timestamp' => time(),
                    );
                    
                    $this->session->set_userdata($userData);
                    
                    redirect(base_url()."Admin");
                }else{
                    //echo "not ok";die;
                    redirect("Login?error=Wrong Infomation");
                }
            }
        }
        
        if(isset($error_msg)){
            $data['error_msg'] = $error_msg;
            $this->load->view('template/login', $data);
        }else{
            $this->load->view('template/login');
        }
        
    }
    
    public static function username_validation($username){        
        
        $username = str_replace(' ', '', $username); /* Replaces all spaces with hyphens. */
        $username = trim($username);  /* Triming */
        $username = str_replace('/\s/g', '', $username); /* White space removing */
        
        /* special character removing */
        if (preg_match('/[\'^£$%&*()}{#~?>@_<>,|=+¬-]/', $username)){
            return FALSE;
        }else{
            return $username;
        }
    }
    
    public function loggout(){
        $this->session->unset_userdata('current_user_username');
        $this->session->unset_userdata('current_user_email');
        $this->session->unset_userdata('current_user_type');
        $this->session->unset_userdata('lang');

        $this->session->sess_destroy();
        redirect('Login');
    }
    
}
