<?php

Class General_model extends CI_Model {
    
    public function query($select, $table, $whr){
        
        $this->db->from($table);
        $this->db->select($select);
        $this->db->where($whr);
        
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->row();
        }else{
            return FALSE;
        }
        
    }
    
    public function insert_data_with_return_id($table, $arr) {
        $result = $this->db->insert($table, $arr);
        $insert_id = $this->db->insert_id();
        if($insert_id){
            return $insert_id;
        }else{
            return FALSE;
        }
    }
    public function insert_data($table, $arr) {
        $result = $this->db->insert($table, $arr);
        
        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function get_data($select='',$table='' ,$whr='', $join='' ,$order_by='',$ac_dc='', $limit='',$start=0) {
        $this->db->select($select);
        $this->db->from($table);
        if ($join) {
            $this->db->join($join[0], $join[1], $join[2]);
        }
        if($whr){
            $this->db->where($whr);
        }
        
        if($order_by){
            $this->db->order_by($order_by,$ac_dc);
        }
        
        if($limit){
            $this->db->limit($limit, $start);
         }
        
        
        $result = $this->db->get();
        
        if($result){
            return $result->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function update_data($table, $userData, $whr) {
        $this->db->set($userData); //value that used to update column  
        $this->db->where($whr); //which row want to upgrade  
        $result = $this->db->update($table);
        
        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
}